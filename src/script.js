var getLocations = function(callback) {
  var decision = "";
  var request = new XMLHttpRequest();
  request.open(
    "GET",
    "https://www.jsonstore.io/66f85a29c7848dd6cf97b626cab8748620943ea8b67981d12c9ce0e6be51daac",
    true
  );

  request.onload = function() {
    if (this.status >= 200 && this.status < 400) {
      // Success!
      var data = JSON.parse(this.response);
      setTimeout(function() {
        callback(data.result);
      }, 1000);
    } else {
      // We reached our target server, but it returned an error
      callback(["I'm afraid I can't do that, Dave."]);
    }
  };

  request.onerror = function() {
    // There was a connection error of some sort
    callback(["I'm afraid I can't do that, Dave."]);
  };

  request.send();
};

// given an array, randomly sort then return the first element
var decide = function(e) {
  var draw = function(locations) {
    e.target.innerText = locations.sort(function() {
      return 0.5 - Math.random();
    })[0];
  };
  return draw;
};

document.getElementById("decide").addEventListener("click", function(e) {
  e.target.innerText = "thinking...";
  getLocations(decide(e));
});
